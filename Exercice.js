import  express  from 'express';

import steam from './SteamGames.json'  assert {type:"json"};



const app = express();


const hostname ='127.0.0.1';
const port=process.env.Port || 9090;
/**
 @param {IncomingMessage} req
 @param {ServerResponse} res
 */
 


 app.get("/game",(req,res)=>{
    
res.status(200).json(steam)
 })

 app.get("/game/select/:year",(req,res)=>{
    
   res.status(200).json(steam.filter(val=>req.params.year<val.Year))
    })

app.get("/game/:name",(req,res)=>{
    
      res.status(200).json({"url":steam.find(val.Game===req.params.name).GameLink})
       })
   
   


 
 app.listen(port,hostname,() =>{
    console.log(`Server running at http://${hostname}:${port}`);
    });


