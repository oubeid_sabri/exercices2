import  express  from 'express';

class Game{
  constructor(name,year){
    this.name = name;
    this.year = year;
  }
}

const app = express();

const hostname ='127.0.0.1';
const port=process.env.Port || 9090;
/**
 @param {IncomingMessage} req
 @param {ServerResponse} res
 */

 app.get('/entity', (req ,res)=> {
    const game = new Game("dmc5", 2022);
    res.status(200).json(game);
    })

 app.get('/', (req,res) => {
res.status(200).json({ message :'hello gamix'});
 })
 
 app.get('/game/:name', (req,res) => {
    res.status(200).json({ message :`the name of this game is ${req.params.name}`});
     })

 app.get("/secret", (req, res)=> {
res.status(401).json({ message: "Unauthorized"});
 });    

 app.listen(port, hostname ,() => {
   console.log(`Server running http://${hostname}:${port}/` ) 
 })
