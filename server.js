import {createServer, ServerResponse} from 'http';


const hostname='127.0.0.1';
const port=process.env.Port || 9090;

/** 

@param {IncomingMessage} req
@param {ServerResponse} res

*/
const server =createServer((req,res) =>{
    res.statusCode =200;
    res.setHeader('Content-Type','text/plain');
    res.end(' Hello Wolrd');

});

server.listen(port,hostname,() =>{
console.log(`Server running at http://${hostname}:${port}`);
});
